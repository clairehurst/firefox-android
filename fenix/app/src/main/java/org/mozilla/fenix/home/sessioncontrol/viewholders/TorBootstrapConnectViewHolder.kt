/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders

import android.view.View
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.TorBootstrapConnectBinding
import org.mozilla.fenix.tor.TorEvents
import org.mozilla.fenix.tor.bootstrap.TorQuickStart
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.home.sessioncontrol.TorBootstrapInteractor

class TorBootstrapConnectViewHolder(
    private val view: View,
    private val components: Components,
    private val interactor: TorBootstrapInteractor
) : RecyclerView.ViewHolder(view), TorEvents {

    var binding: TorBootstrapConnectBinding

    init {
        binding = TorBootstrapConnectBinding.bind(view)
        val torQuickStart = TorQuickStart(view.context)
        setQuickStartDescription(view, torQuickStart)

        with(binding.quickStartToggle) {
            setOnCheckedChangeListener { _, isChecked ->
                torQuickStart.setQuickStartTor(isChecked)
                setQuickStartDescription(view, torQuickStart)
            }

            isChecked = torQuickStart.quickStartTor()
        }

        with(binding.torBootstrapNetworkSettingsButton) {
            setOnClickListener {
                interactor.onTorStopBootstrapping()
                interactor.onTorBootstrapNetworkSettingsClicked()

                with(binding.torBootstrapProgress) {
                    visibility = View.INVISIBLE
                }

                with(binding.torBootstrapConnectButton) {
                    visibility = View.VISIBLE
                }
            }
        }

        with(binding.torBootstrapConnectButton) {
            setOnClickListener {
                interactor.onTorBootstrapConnectClicked()
                interactor.onTorStartBootstrapping()

                visibility = View.INVISIBLE

                with(binding.torBootstrapProgress) {
                    visibility = View.VISIBLE
                }
            }
        }

        components.torController.registerTorListener(this)
    }

    private fun setQuickStartDescription(view: View, torQuickStart: TorQuickStart) {
        val resources = view.context.resources
        val appName = resources.getString(R.string.app_name)
        if (torQuickStart.quickStartTor()) {
            binding.torBootstrapQuickStartDescription.text = resources.getString(
                R.string.tor_bootstrap_quick_start_enabled, appName
            )
        } else {
            binding.torBootstrapQuickStartDescription.text = resources.getString(
                R.string.tor_bootstrap_quick_start_disabled
            )
        }
    }

    @SuppressWarnings("EmptyFunctionBlock")
    override fun onTorConnecting() {
    }

    override fun onTorConnected() {
        components.torController.unregisterTorListener(this)
    }

    @SuppressWarnings("EmptyFunctionBlock")
    override fun onTorStopped() {
    }

    override fun onTorStatusUpdate(entry: String?, status: String?) {
        if (entry == null) return

        binding.torBootstrapStatusMessage.text = entry
        if (entry.startsWith(BOOTSTRAPPED_PREFIX)) {
            val percentIdx = entry.indexOf("%")
            val percent = entry.substring(
                BOOTSTRAPPED_PREFIX.length,
                percentIdx
            )
            with(binding.torBootstrapProgress) {
                progress = percent.toInt()
            }
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_bootstrap_connect
        const val BOOTSTRAPPED_PREFIX = "NOTICE: Bootstrapped "
    }
}
